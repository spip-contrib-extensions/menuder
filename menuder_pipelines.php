<?php

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;


function menuder_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" href="'.timestamp(direction_css(find_in_path('css/menuder.css'))).'" type="text/css" media="projection, screen" />';
	return $flux;
}


